﻿%Program written in MatLab
%Creator: Mateusz Niznik

fileId1=fopen('C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-08.csv','r');
fileId2=fopen('C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-09.csv','r');
fileId3=fopen('C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-10.csv','r');
fileId4=fopen('C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-11.csv','r');
fileId5=fopen('C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-12.csv','r');
fileId6=fopen('C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-13.csv','r');
fileId7=fopen('C:\Users\student\Desktop\data\dane-pomiarowe_2019-10-14.csv','r');

pole1 = textscan(fileId1,'%q %q %q %q %q %q','Delimiter',';');
pole2 = textscan(fileId2,'%q %q %q %q %q %q','Delimiter',';');
pole3 = textscan(fileId3,'%q %q %q %q %q %q','Delimiter',';');
pole4 = textscan(fileId4,'%q %q %q %q %q %q','Delimiter',';');
pole5 = textscan(fileId5,'%q %q %q %q %q %q','Delimiter',';');
pole6 = textscan(fileId6,'%q %q %q %q %q %q','Delimiter',';');
pole7 = textscan(fileId7,'%q %q %q %q %q %q','Delimiter',';');

fclose(fileId1);
fclose(fileId2);
fclose(fileId3);
fclose(fileId4);
fclose(fileId5);
fclose(fileId6);
fclose(fileId7);

for j=2:25
p1_val{j-1}=str2num(pole1{6}{j});
p2_val{j-1}=str2num(pole2{6}{j});
p3_val{j-1}=str2num(pole3{6}{j});
p4_val{j-1}=str2num(pole4{6}{j});
p5_val{j-1}=str2num(pole5{6}{j});
p6_val{j-1}=str2num(pole6{6}{j});
p7_val{j-1}=str2num(pole7{6}{j});
end

M1=cell2mat(p1_val);
M2=cell2mat(p2_val);
M3=cell2mat(p3_val);
M4=cell2mat(p4_val);
M5=cell2mat(p5_val);
M6=cell2mat(p6_val);
M7=cell2mat(p7_val);

max1=0;
max2=0;
max3=0;
max4=0;
max5=0;
max6=0;
max7=0;

for j=1:24
if M1(j)>max1
max1=M1(j);
zmien1=j;
end
if M2(j)>max2
max2=M2(j);
zmien2=j;
end
if M3(j)>max3
max3=M3(j);
zmien3=j;
end
if M4(j)>max4
max4=M4(j);
zmien4=j;
end
if M5(j)>max5
max5=M5(j);
zmien5=j;
end
if M6(j)>max6
max6=M6(j);
zmien6=j;
end
if M7(j)>max7
max7=M7(j);
zmien7=j;
end
end

godz1=pole1{1}{zmien1+1};
godz2=pole2{1}{zmien2+1};
godz3=pole3{1}{zmien3+1};
godz4=pole4{1}{zmien4+1};
godz5=pole5{1}{zmien5+1};
godz6=pole6{1}{zmien6+1};
godz7=pole7{1}{zmien7+1};

val_max=[max1,max2,max3,max4,max5,max6,max7];
godz={godz1,godz2,godz3,godz4,godz5,godz6,godz7};
date={'2019-10-08','2019-10-09','2019-10-10','2019-10-11','2019-10-12','2019-10-13','2019-10-14'};

max1_7=0;

for j=1:7
if val_max(j)>max1_7
max1_7=val_max(j);
zmien8=j;
end
end

fileID = fopen('PMresults.dat','w'); %this file is normaly created in C:\Users\student\Documents\MATLAB

fprintf ('Maximum concentration of PM10 [µg/m3] in week (08-14.10.2019) was at %s %s, value: %d \n',date{zmien8},godz{zmien8},max1_7);
fprintf (fileID, 'Maximum concentration of PM10 [µg/m3] in week (08-14.10.2019) was at %s %s, value: %d \n',date{zmien8},godz{zmien8},max1_7);

%time range was taken from https://meteogram.pl/slonce/polska/gliwice/
    %day: 8:00 - 17:00
    %night: 1:00 - 7:00, 18:00 - 24:00

sred1d=0;
sred2d=0;
sred3d=0;
sred4d=0;
sred5d=0;
sred6d=0;
sred7d=0;

sred1n=0;
sred2n=0;
sred3n=0;
sred4n=0;
sred5n=0;
sred6n=0;
sred7n=0;

for j=7:18
sred1d=sred1d+M1(j);
sred2d=sred2d+M2(j);
sred3d=sred3d+M3(j);
sred4d=sred4d+M4(j);
sred5d=sred5d+M5(j);
sred6d=sred6d+M6(j);
sred7d=sred7d+M7(j);
end

sred1d=sred1d/12;
sred2d=sred2d/12;
sred3d=sred3d/12;
sred4d=sred4d/12;
sred5d=sred5d/12;
sred6d=sred6d/12;
sred7d=sred7d/12;

for j=1:6
sred1n=sred1n+M1(j);
sred2n=sred2n+M2(j);
sred3n=sred3n+M3(j);
sred4n=sred4n+M4(j);
sred5n=sred5n+M5(j);
sred6n=sred6n+M6(j);
sred7n=sred7n+M7(j);
end

for j=19:24
sred1n=sred1n+M1(j);
sred2n=sred2n+M2(j);
sred3n=sred3n+M3(j);
sred4n=sred4n+M4(j);
sred5n=sred5n+M5(j);
sred6n=sred6n+M6(j);
sred7n=sred7n+M7(j);
end

sred1n=sred1n/12;
sred2n=sred2n/12;
sred3n=sred3n/12;
sred4n=sred4n/12;
sred5n=sred5n/12;
sred6n=sred6n/12;
sred7n=sred7n/12;

sredn=[sred1n,sred2n,sred3n,sred4n,sred5n,sred6n,sred7n];
sredd=[sred1d,sred2d,sred3d,sred4d,sred5d,sred6d,sred7d];

dn=0;

for j=1:7
if sredn(j)>sredd(j)
dn=dn+1;
else
dn=dn-1;
end
end

if dn>2
fprintf ('There was a statistical diffrence between night and day concentration of PM10. In %d days there was higher concentration at night than at day',dn);
fprintf (fileID, 'There was a statistical diffrence between night and day concentration of PM10. In %d days there was higher concentration at night than at day',dn);
end

if -2<dn<2
fprintf ('There wasnt statistical diffrence between night and day concentration of PM10.');
fprintf (fileID, 'There wasnt statistical diffrence between night and day concentration of PM10.');
else
fprintf ('There was a statistical diffrence between night and day concentration of PM10. In %d days there was higher concentration at day than at night',abs(dn));
fprintf (fileID, 'There was a statistical diffrence between night and day concentration of PM10. In %d days there was higher concentration at day than at night',abs(dn));
end

fclose(fileID);



